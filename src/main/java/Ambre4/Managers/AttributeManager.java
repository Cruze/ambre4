package Ambre4.Managers;

import Ambre4.Model.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Set;

/**
 * User: Алексей
 * Date: 12.12.2014
 * Time: 17:54
 */
public class AttributeManager {

    private static AttributeManager INSTANCE;

    private HashMap<String, Class<? extends Attribute>> attributeMap = new HashMap<String, Class<? extends Attribute>>();

    public AttributeManager() {
        init();
    }

    private void init() {
        Reflections reflections = new Reflections("Ambre4.Model.Attributes");

        Set<Class<? extends Attribute>> allClasses = reflections.getSubTypesOf(Attribute.class);
        for (Class<? extends Attribute> cl : allClasses) {
            try {
                Method m = cl.getMethod("getGroupName");
                Object result = m.invoke(null);
                attributeMap.put((String)result, cl);
            }
            catch (Exception e) {
                System.out.println("Not found valid method getGroupName for class " + cl.getCanonicalName());
            }
        }
    }

    public Attribute createAttribute(Element el) {
        String[] parsed = parseAttribute(el);

        Class<? extends Attribute> cl = attributeMap.get(parsed[0]);
        if (cl != null) {
            try {
                Constructor<?> cons = cl.getConstructor(String.class, String.class);
                Object object = cons.newInstance(parsed[0], parsed[1]);
                return (Attribute) object;
            } catch (Exception e) {
                System.out.println("Not found valid constructor for class " + cl);
                return null;
            }
        }
        else {
            return new Attribute(parsed[0], parsed[1]);
        }
    }

    private String[] parseAttribute(Element el) {
        String[] result = new String[2];
        result[0] = el.select(".field-label").text().trim();

        // TODO: перенести в класс Attribute
        result[1] = "";
        Elements values = el.select(".links li");
        boolean first = true;
        for (Element val : values) {
            if (first)
                first = false;
            else
                result[1] += ",";
            result[1] += val.text().trim();
        }
        return result;
    }



    public static AttributeManager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new AttributeManager();
        return INSTANCE;
    }

}
