package Ambre4.Parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Cruze on 30.11.2014.
 * this class parse page
 */
public class Parser {



    public ArrayList<String> getCategoryURL(String page){
        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        ArrayList<String> categoryURL = new ArrayList<String>();
        Elements elements = doc.select("ul[id=main-menu-links]").select("li");
        for (Element element : elements) {
            categoryURL.add(element.select("a").attr("href"));
        }
        return categoryURL;
    }

    public ArrayList<String> getURLFromPage(String page) {
        ArrayList<String> urls = new ArrayList<String>();
        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        Elements elements = doc.select("div[id=content]").select("div[class=field-content]");
        System.out.println(elements.text());
        for (Element element : elements) {
            urls.add(element.select("a").attr("href"));
        }
        return urls;
    }

    public int getCount(String page) {
        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        String string = doc.select("li[class=pager-current").text();
        if(!string.equals("")) {
            return Integer.parseInt(string.split(" ")[2]);
        } else {
            return 0;
        }
    }
}
