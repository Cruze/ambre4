package Ambre4;

import Ambre4.Model.Category;
import Ambre4.Model.Item;
import Ambre4.NetWorker.NetWorker;
import Ambre4.Parser.Parser;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Created by Cruze on 30.11.2014.
 * main class
 */
public class Main {

    private static final String mainURL = "http://ambre4u.ru";
    private static NetWorker netWorker = new NetWorker();;
    private static Parser parser = new Parser();
    private static String firstString = "_ID_;_CATEGORY_;_NAME_;_MODEL_;_MANUFACTURER_;_IMAGE_;";

    public static void main(String[] args) {
        System.out.println("Begin");
        ArrayList<String> categoryURL = getCategoryURL();
        System.out.println("categoryURL was get");
        ArrayList<Category> categories = new ArrayList<Category>();
        System.out.println("begin work with urls");

        for (String url : categoryURL) {
            System.out.println("url: " + url);
            Category category = new Category(url);
            int count = parser.getCount(netWorker.getPage(url));
            if (count == 0) {
                category.add(url);
            }
            for (int i = 0; i < count; i++) {
                System.out.println("№: " + i);
                String s = url + "?field_manufacturer_tid=All&page=" + i ;
                System.out.println(s);
                category.add(s);
            }
            categories.add(category);
            System.out.println(category);

        }
        System.out.println("getting url for each item");
        ArrayList<Category> categoryWithItemURL = new ArrayList<Category>();
        for (Category category : categories) {
            Category c = new Category(category.getName());
            for (String url : category.getUrls()) {
                String page = netWorker.getPage(url);
                ArrayList<String> urls = parser.getURLFromPage(page);
                c.add(urls);
            }
            categoryWithItemURL.add(c);
        }

        for (Category category : categoryWithItemURL) {
            String fileName = category.getName().substring(18);
            File file = new File(fileName);
            try {
                FileWriter fileWriter = new FileWriter(file, false);
                System.out.println("Create file: " + fileName);
                fileWriter.write(firstString + "\n");
                for (String url : category.getUrls()) {
                    Item item = new Item(netWorker.getPage(mainURL + url));
                    item.load();
                    fileWriter.write(item.toString() + "\n");
                    // System.out.println("serialize start: " + item.getName());
                    // item.serialize(directory); vv
                    fileWriter.flush();
                }
                fileWriter.close();
            }
            catch (Exception e) {
                System.out.println("error while writing file \"" + fileName + "\"");
            }
        }
        System.out.println("End");
    }

    private static ArrayList<String> getCategoryURL() {
        ArrayList<String> categoryURL =  parser.getCategoryURL(netWorker.getPage(mainURL));
        ArrayList<String> urls = new ArrayList<String>();
        for (String s : categoryURL) {
            urls.add(mainURL + s);
        }
        return urls;
    }

}
