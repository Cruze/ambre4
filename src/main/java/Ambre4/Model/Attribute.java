package Ambre4.Model;

/**
 * User: Алексей
 * Date: 10.12.2014
 * Time: 0:13
 */
public class Attribute implements IModel{

    protected static String GROUP_NAME;

    public String getName() {
        return _name;
    }

    public String getValue() {
        return _value;
    }

    // for print
    private String _name;
    private String _value;

    public Attribute(String name, String value) {
        _name = name;
        _value = value;
    }

    public boolean load() {
        return true;
    }

    public String toString() {
        return _value;
    }


    public static String getGroupName() {
        return GROUP_NAME;
    }
}
