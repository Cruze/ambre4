package Ambre4.Model;

import Ambre4.Managers.AttributeManager;
import Ambre4.NetWorker.NetWorker;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Cruze on 30.11.2014.
 * model for 1 item
 */
public class Item {

    private String _page;

    // for print
    private String _name;
    private String _price;
    private IModel _image;
    private ArrayList<IModel> _articles = new ArrayList<IModel>();
    private ArrayList<IModel> _attributes = new ArrayList<IModel>();

    public static void main(String[] args) {
        NetWorker netWorker = new NetWorker();
        Item item = new Item(netWorker.getPage("http://ambre4u.ru/amouage/amouage-gold"));
        item.load();
        System.out.println(item);
    }

    public Item(String page) {
        _page = page;
    }

    public boolean load() {
        org.jsoup.nodes.Document doc = Jsoup.parse(_page);
        Elements elements = doc.select("div[class=section]");
        _name = elements.select("h1[class=title]").text();
        _price = getPrice(elements.select(".commerce-product-field-commerce-price").text());
        Elements descriptions = elements.select("#all-attributes div.field");
        try {
            IModel attribute;
            for (int i = 0; i < descriptions.size() - 1; i++) {
                attribute = AttributeManager.getInstance().createAttribute(descriptions.get(i));
                attribute.load(); // not need
                _attributes.add(attribute);
            }
        } catch(Exception e){
            System.out.println("exception was: " + _name);
        }
        Element element = doc.select("div[class=view-content]").get(2);
        Elements articles = element.select("tr");
        for (Element art : articles) {
            IModel article = new Article(art.select("td"));
            article.load();
            _articles.add(article);
        }
        String imageURL = elements.select("div[id=aromat-image]").select("img").attr("src");
        _image = new Image(imageURL);
        _image.load();

        return true;
    }

    private String getPrice(String text) {
        return text.replaceAll("\\D", "");
    }

    public String getName() {
        return _name;
    }

    @Override
    public String toString() {
        //_CATEGORY_;_ID_;_NAME_;_MODEL_;_MANUFACTURER_;_PRICE_;_DESCRIPTION_;_IMAGE_

        /*
        StringBuilder builder = new StringBuilder("0;"); // set id
        builder.append("\"").append(_name).append("\"").append(";");
        builder.append(_price).append(";");
        builder.append("\"").append(_image).append("\"").append(";");
        for (IModel attribute : _attributes) {
            builder.append("\"").append(attribute).append("\"").append(";");
        }
        */

        String description = getDescritpionHtml();

        StringBuilder builder = new StringBuilder("");
        for (IModel iModel :  _articles) {
            Article article = (Article) iModel;
            builder.append("\"").append("Парфюмерия|").append(_attributes.get(1)).append("\"").append(";"); //category
            builder.append("0").append(";"); //id
            builder.append("\"").append(_name).append(" ").append(article.getDescription()).append("\"").append(";"); //name
            builder.append("\"").append(_name).append("\"").append(";"); //model
            builder.append("\"").append(_attributes.get(0)).append("\"").append(";"); //manufacture
            builder.append(getPrice(article.getPrice())).append(";"); //price
            builder.append("\"").append(description).append("\"").append(";"); //description
            builder.append("\"").append("data/perfumery/").append(_image).append("\"").append("\n");
        }
        return builder.toString();
    }

    private String getDescritpionHtml() {
        StringBuilder builder = new StringBuilder("<table class=\"\"b-properties\"\" xmlns:lego=\"\"https://lego.yandex-team.ru\"\">").append("\n");
        builder.append("<tbody>").append("\n");
        for (IModel attribute : _attributes) {
            builder.append(getAttributeHtml((Attribute) attribute));
        }
        builder.append("</tbody>").append("\n");
        builder.append("</table>").append("\n");
        builder.append("<p>").append("\n");
        builder.append("&nbsp;</p>").append("\n");
        return builder.toString();
    }

    private String getAttributeHtml(Attribute attribute) {
        StringBuilder builder = new StringBuilder("<tr>").append("\n");
        builder.append("<th class=\"\"b-properties__label b-properties__label-title\"\">").append("\n");
        builder.append(attribute.getName()).append("</th>").append("\n");
        builder.append("<td class=\"\"b-properties__value\"\">").append("\n");
        builder.append(attribute.getValue()).append("</td>").append("\n");
        builder.append("</tr>").append("\n");
        return builder.toString();
    }


}
