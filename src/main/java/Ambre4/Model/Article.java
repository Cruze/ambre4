package Ambre4.Model;

import org.jsoup.select.Elements;

/**
 * Created by Cruze on 02.12.2014.
 * Article has a number, description and price
 */
public class Article implements IModel {

    private Elements _elements;

    // for print
    private String _name;
    private String _description;

    public String getPrice() {
        return _price;
    }

    private String _price;

    public Article(Elements elements) {
        _elements = elements;
    }


    public String getDescription() {
        return _description;
    }
    public boolean load() {
        _name = _elements.get(0).text().trim();
        _description = _elements.get(1).text().trim();
        _price = _elements.get(2).text().trim();
        return true;
    }

    @Override
    public String toString() {
        return "";
    }
}
