package Ambre4.Model.Attributes;

import Ambre4.Model.Attribute;

/**
 * User: Алексей
 * Date: 12.12.2014
 * Time: 18:02
 */
public class Group extends Attribute {

    static {
        GROUP_NAME = "Пол:";
    }

    public Group(String name, String value) {
        super(name, value);
    }

    public static String getGroupName() {
        return GROUP_NAME;
    }
}
