package Ambre4.Model;

import java.util.ArrayList;

/**
 * Created by Cruze on 02.12.2014.
 * this class contain 1 categoru and urls for each item of this category
 */
public class Category {
    private String name;
    ArrayList<String> urls = new ArrayList<String>();

    public Category(String name) {
        this.name = name;
    }
    public void add(String url){
        urls.add(url);
    }
    public void add(ArrayList<String> arrayList){
        urls.addAll(arrayList);
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", urls=" + urls +
                '}';
    }

    public String getName() {
        return name;
    }
}
