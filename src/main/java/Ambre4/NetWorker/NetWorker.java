package Ambre4.NetWorker;



import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by Cruze on 30.11.2014.
 * this class work with net
 */
public class NetWorker {

    public String getPage(String url){
        System.out.println("getPage: " + url);
        HttpClient httpClient = HttpClientBuilder.create().build() ;
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response;
        String page = "";
        try {
            response = httpClient.execute(httpGet);
            page = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return page;
    }
}
