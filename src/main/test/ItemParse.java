import Ambre4.Model.Item;
import Ambre4.NetWorker.NetWorker;
import org.junit.Before;
import org.junit.Test;

/**
 * User: Алексей
 * Date: 09.12.2014
 * Time: 23:54
 */
public class ItemParse {

    private String compare;

    @Before
    public void init() {
        compare = "0;Amouage Library Collection: Opus VIII;17070;amouage_library_collection_opus_viii.jpg;для нее;2014;восточные,древесные;иланг-иланг,листья жасмина,флердоранж;ветивер,дерево гуаяк,имбирь,ладан,шафран;бензоин;";
    }

    @Test
    public void parse() {
        NetWorker netWorker = new NetWorker();
        Item item = new Item(netWorker.getPage("http://ambre4u.ru/amouage/amouage-library-collection-opus-viii"));
        item.load();
        System.out.println(item.toString());
        //assertEquals(item.toString(), compare);
    }
}
